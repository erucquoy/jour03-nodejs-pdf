import axios from 'axios'
import fs from 'fs'

axios.get('https://images.unsplash.com/photo-1645655144330-08d6ae83054b?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&dl=jordan-rowland-kCgrb5VACLg-unsplash.jpg', {
    responseType: 'stream'
})
.then(response => {
    response.data.pipe(fs.createWriteStream('mon_image.jpg'))
})