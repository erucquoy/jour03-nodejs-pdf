import axios from 'axios'

const BEARER_TOKEN = 'Bonjour'

// creer une nouvelle instance d'axios
const api = axios.create({
    baseURL: 'https://api.coingecko.com/api/v3',
    timeout: 3000,
    validateStatus: () => { return true },
    // headers: {
    //     'Authorization': `Bearer ${BEARER_TOKEN}`
    // }
})

class CoinApi {
    http; // instance axios
    vs_curr;
    constructor(vs_curr = 'USD') {
        this.vs_curr = vs_curr
        this.http = axios.create({
            baseURL: 'https://api.coingecko.com/api/v3',
            timeout: 3000,
            validateStatus: () => { return true },
        })
    }

    simplePrice(ids = ['bitcoin']) {
        return api.get(`/simple/price`, {
            params: {
                ids: ids.join(','),
                vs_currencies: this.vs_curr
            }
        })
    }

    getCoinById(id) {
        return api.get(`/coins/${id}/tickers`)
    }

    getCoinsList() {
        return api.get(`/coins/list`)
    }

}

function showData({ data }) {
    console.log(data);
}

const apiUSD = new CoinApi('USD')
const apiEUR = new CoinApi('EUR')

apiUSD.simplePrice(['bitcoin', 'ethereum']).then(showData)
apiEUR.simplePrice(['bitcoin', 'ethereum']).then(showData)




