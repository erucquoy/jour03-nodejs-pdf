import { Readable, Writable, Duplex } from 'stream'

const duplex = new Duplex({
    defaultEncoding: 'utf8',
    write(chunk, encoding, cb) {
        this.push(chunk)
    },
    read(size) {
        console.log('size:', size);
    }
})

if (duplex.writable) {
    duplex.push('Bonjour everyone !', 'utf8')
}

duplex.on('data', (chunk) => {
    console.log(chunk.toString('utf8'));
})

if (duplex.writable) {
    duplex.push('Bonjour everyone ! (2)')
}

setTimeout(() => {
    if (duplex.writable) {
        duplex.push('Bonjour everyone ! (timeout)')
    }
}, 1000)