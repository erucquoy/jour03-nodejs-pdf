
// __dirname
import path from 'path';
import { fileURLToPath } from 'url';
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

// i2pdf 
import imagesToPdf from 'images-to-pdf'

export function i2pdf(images_array = [], pathPDF) {
    return imagesToPdf(images_array, pathPDF)
}

import fs from 'fs/promises'

function testFunc() {
    fs.readdir(path.resolve(__dirname, '..', 'data', 'images')).then(files => {
        files = files.filter(fileName => fileName != '.gitkeep')
        files = files.map(fileName => path.resolve(__dirname, '..', 'data', 'images', fileName))
        i2pdf(files, path.resolve(__dirname, '..', 'data', 'pdfs', 'test.pdf'))
    })
}
// testFunc()