import { workerData, parentPort } from 'worker_threads'
import { resolve } from 'path'
import { i2pdf } from './i2pdf.js'

const PATH_IMAGES = resolve('..', 'data', 'images')
const PATH_PDFS = resolve('..', 'data', 'pdfs')

const gen_path_i = (f) => resolve(PATH_IMAGES, f)
const gen_path_pdf = (f) => resolve(PATH_PDFS, f)

// crée un pdf
function work(pdf_id, nb_img) {
    const files = generate_array_files(pdf_id, nb_img)
    const pdfPath = gen_path_pdf(`${pdf_id}.pdf`)
    i2pdf(files, pdfPath)
}

// genere un tableau de chemins vers les fichiers images pour 1 .pdf
function generate_array_files(pdf_id, nb_img) {
    const arr = []
    for (let i = (0 + (pdf_id * nb_img)); i < ((pdf_id * nb_img) + nb_img); i++) {
        arr.push(gen_path_i(`${i}.jpg`))
    }
    return arr
}

// lancement du worker
// recuperation des workerData
const { count: countPDF, start, IMG_PER_PDF } = workerData

// pour chaque .pdf (count) on lance un work() avec le bon id (i + start)
for(let i = start; i < (countPDF + start); i++) {
    work(i, IMG_PER_PDF)
}