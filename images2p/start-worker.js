import { Worker } from 'worker_threads'
import os from 'os'

const NB_PDF = 128
const IMG_PER_PDF = 8
const NB_WORKER = os.cpus().length

const count = NB_PDF / NB_WORKER // nb pdf / worker

for (let i = 0; i < NB_WORKER; i++) {
    const start = i * count; // START OF PDF ID --- start 0, 4, 8 ... (if count = 4)
    const worker = new Worker('./worker-pdf.js', {
        workerData: {
            count, start, IMG_PER_PDF
        }
    })
    worker.on('exit', (code) => {
        console.log(`Worker ${start} exited with code ${code}`);
    })
    worker.on('error', (err) => {
        console.error(err);
    })
}