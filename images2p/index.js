const now = +new Date();
import './i2pdf.js'

process.on('exit', (code) => {
    console.log('PDF generated');
    const exited = +new Date();
    const execTime = (exited - now)
    console.log({ execTime, now, exited });
})