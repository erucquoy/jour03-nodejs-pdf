// on est dans le worker
import { workerData, parentPort } from 'worker_threads'
import { generateImages } from './gen-image.js'

// parentPort pour communiquer avec le thread principal
parentPort.postMessage({
    type: 'START-WORKER',
    message: 'Good to see u'
})

generateImages(workerData.count, workerData.start)
    .then(() => {
        console.log(`${workerData.count} generated : FROM ${workerData.start} TO ${workerData.start + workerData.count}`);
    })