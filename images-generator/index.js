const now = +new Date();
import { generateImages } from './gen-image.js'
generateImages(100)
    .then(res => {
        console.log(res);
    })

process.on('exit', (code) => {
    console.log('Images generated');
    const exited = +new Date();
    const execTime = (exited - now)
    console.log({ execTime, now, exited });
})