import { Worker } from 'worker_threads'
import os from 'os'

const NB_IMAGES = 1024
const NB_WORKER = 10
const NB_WORKER_BIS = os.cpus().length

console.log(NB_WORKER_BIS, NB_IMAGES / 16);


function start_images_workers() {

    for(let i = 0; i < NB_WORKER_BIS; i++) {
        let ic = i;
        console.log(`STARTING WORKER ${ic} : FROM ${i * (NB_IMAGES / NB_WORKER_BIS)} TO ${(i * (NB_IMAGES / NB_WORKER_BIS)) + (NB_IMAGES / NB_WORKER_BIS)}`);
        const worker = new Worker('./worker-gen.js', {
            workerData: {

                count: NB_IMAGES / NB_WORKER_BIS,
                start: i * (NB_IMAGES / NB_WORKER_BIS)
            }
        })
        worker.on('exit', (code) => {
            console.log(`Worker ${ic} finished with code ${code}`);
        })
        worker.on('error', (err) => {
            console.error(`Worker ${ic} ERRORED:${err.message}`);
        })
        worker.on('message', data => {
            console.log(`Worker ${ic} sent a message:`, data);
        })
    }

}

start_images_workers()