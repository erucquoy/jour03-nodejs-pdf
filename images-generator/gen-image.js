import path from 'path';
import { fileURLToPath } from 'url';
import { generateImage as generateImageCb } from 'js-image-generator'
import { writeFile } from 'fs/promises';

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

const IMAGES_PATH = path.resolve(__dirname, '..', 'data', 'images')


// promisify la fonction generateImageCb 
export function generateImage(width, height, quality) {
    return new Promise((resolve, reject) => {
        generateImageCb(width, height, quality, (err, image) => {
            if (err) reject(err)
            resolve(image)
        })
    })
}

export function generateImages(count = 10, range = 0) {
    const promises = []
    for(let i = (0 + range); i < (count + range); i++) {
        let ic = i;
        promises.push(
            generateImage(800, 600, 80)
                .then(image => writeFile(`${IMAGES_PATH}/${ic}.jpg`, image.data))
        );
    }
    return Promise.all(promises)
}
