const now = +new Date();
import { Worker } from 'worker_threads'
import fs from 'fs'

const dataStream = fs.createWriteStream('userdatas.dat')

const NB_WORKER = 1
const NB_TO_GEN = 5000000

const workers = []

for (let i = 0; i < NB_WORKER; i++) {
    const worker = new Worker('./worker-data-gen.js', {
        workerData: {}
    })
    worker.on('exit', worker_exit)
    worker.on('error', worker_error)
    worker.on('message', handleData)
    workers.push(worker)
}

let dataReceived = 0
function handleData(data) {
    if (data.type && data.type == 'UserData') {
        dataStream.write(`${Object.values(data.data).join(';')}\n`, (err) => {
            dataReceived++;
            if (dataReceived >= NB_TO_GEN) {
                const beforeExit = +new Date()
                console.log((beforeExit - now));
                process.exit(0)
            }
        })
    }
    if (data.type && data.type == 'UserDatas') {
        let _bigStr = (data.data.map(userData => Object.values(userData).join(';'))).join('\n')
        dataStream.write(_bigStr, (err) => {
            dataReceived += data.data.length;
            if (dataReceived >= NB_TO_GEN) {
                const beforeExit = +new Date()
                console.log((beforeExit - now));
                process.exit(0)
            }
        })
    }
}

function worker_exit(code) {
    console.log('Worker exited with code', code);
}

function worker_error(err) {
    console.error(err);
}

function start_working() {
    const per_worker = NB_TO_GEN / NB_WORKER
    for(let worker of workers) {
        worker.postMessage({
            type: 'genData',
            count: per_worker
        })
    }
}
start_working()

// test
// setTimeout(() => {
//     workers[0].postMessage({
//         type: 'genData',
//         count: 1
//     })
// }, 2000)