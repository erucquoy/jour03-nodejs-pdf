import Chance from 'chance'
const chance = new Chance()

export default class UserData {
    constructor() {
        this.name = chance.name()
        this.age = chance.age()
        this.country = chance.country()
        this.money = chance.floating({ min: 100, max: 1000000 })
        this.cc_type = chance.cc_type()
    }
}