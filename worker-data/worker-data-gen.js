import { parentPort } from 'worker_threads'
import UserData from './UserData.js'

const NB_CACHE = 100

parentPort.on('message', (data) => {
    console.log(data);
    if (data.type && data.type == 'genData' && data.count && data.count > 0) {
        genData(data.count)
    }
})


function genData(count = 0) {
    const userDatas = []
    for(let i = 0; i < count; i++) {
        if (userDatas.length >= NB_CACHE) {
            parentPort.postMessage({
                type: 'UserDatas',
                data: userDatas
            })
            userDatas.splice(0, userDatas.length)
        }
        userDatas.push(new UserData())
    }
    parentPort.postMessage({
        type: 'UserDatas',
        data: userDatas
    })
}