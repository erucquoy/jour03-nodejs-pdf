import mysql from 'mysql2/promise'

const pool = mysql.createPool({
    host: '127.0.0.1',
    database: 'company_life',
    user: 'root',
    password: ''
})

async function fetchUsers() {
    const [rows, fields] = await pool.query("SELECT * FROM users")
    console.log({ rows });
}

fetchUsers()