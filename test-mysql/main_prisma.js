import pClient from '@prisma/client'
const { PrismaClient } = pClient

const prisma = new PrismaClient()

async function main() {
  // ... you will write your Prisma Client queries here

    const newUser = await prisma.user.create({
      data: {
        name: 'Test test',
        email: 'test@test.test',
        password: 'testtest',
        Post: {
            create: { title: 'Le Titre !' }
        }
      }
    })
    console.log({ newUser });

    const allUsers = await prisma.user.findMany({
        include: {
          Post: true,
        }
    })

    console.log({ allUsers });
}

main()
  .catch((e) => {
    throw e
  })
  .finally(async () => {
    await prisma.$disconnect()
  })