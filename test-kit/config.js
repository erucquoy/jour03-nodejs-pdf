// __dirname
import path from 'path';
import { fileURLToPath } from 'url';
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

export const IMAGES_PATH = path.resolve(__dirname, '..', 'data', 'images')
export const PDFS_PATH = path.resolve(__dirname, '..', 'data', 'pdfs')

export const gen_pdfPath = (fileName) => path.resolve(PDFS_PATH, fileName)
export const gen_imgPath = (fileName) => path.resolve(IMAGES_PATH, fileName)

export const testImagePath = path.resolve(IMAGES_PATH, '0.jpg')